#include "circulo.hpp"
#include "quadrado.hpp"
#include "formageometrica.hpp"
#include "triangulo.hpp"
#include "paralelogramo.hpp"
#include "pentagono.hpp"
#include "hexagono.hpp"

#include <iostream>
#include <string>
#include <vector>

using namespace std;

string getString(){
    string valor;
    getline(cin, valor);
    return valor;
}

template <typename T1>

T1 getInput(){
    while(true){
    T1 valor;
    cin >> valor;
    if(cin.fail()){
        cin.clear();
        cin.ignore(32767,'\n');
        cout << "Entrada inválida! Insira novamente: " << endl;
    }
    else{
        cin.ignore(32767,'\n');
        return valor;
    }
    }
}

int main(){

    vector<FormaGeometrica * > formasgeometricas;
    vector<quadrado * > quadrados;
    vector<circulo *> circulos;
    vector<triangulo *> triangulos;
    vector<paralelogramo *> paralelogramos;
    vector<pentagono *> pentagonos;
    vector<hexagono *> hexagonos;


    int comando = -1;
    cout << "Formas Geométricas" <<  endl;

    while(comando != 0){
        system("clear");
        cout << "Inserir formas geométricas: " << endl;
        cout << "(1) Inserir forma geométrica" << endl;
        cout << "(2) Inserir quadrado" << endl;
        cout << "(3) Inserir circulo" << endl;
        cout << "(4) Inserir triangulo" << endl;
        cout << "(5) Inserir paralelogramo" << endl;
        cout << "(6) Inserir pentagono" << endl;
        cout << "(7) Inserir hexagono" << endl;
        cout << "(8) Imprime lista geral" << endl;
        cout << "(0) Sair" << endl;
        cout << ":" << endl;
        comando = getInput<int>();

        string tipo;
        float base;
        float altura;

        switch(comando){
            case 1:
                cout << "Cadastre a forma geometrica:" << endl;

                cout << "Tipo: ";
                tipo = getString();

                cout << "Base: ";
                base = getInput<float>();

                cout << "Altura: ";
                altura = getInput<float>();

                formasgeometricas.push_back(new FormaGeometrica(tipo, base, altura));
                break;
            case 2:
                cout << "Cadastre a forma geometrica (quadrado):" << endl;

                cout << "Tipo: ";
                tipo = getString();

                cout << "Base: ";
                base = getInput<float>();

                cout << "Altura: ";
                altura = getInput<float>();

                formasgeometricas.push_back(new quadrado(tipo, base, altura));
                quadrados.push_back(new quadrado(tipo, base, altura));
                break;
            case 3:
                float raio;
                cout << "Cadastre a forma geometrica (circulo):" << endl;

                cout << "Tipo: ";
                tipo = getString();

                cout << "Base: ";
                base = getInput<float>();

                cout << "Altura: ";
                altura = getInput<float>();

                cout << "Raio: ";
                raio = getInput<float>();

                formasgeometricas.push_back(new circulo(tipo, base, altura,raio));
                circulos.push_back(new circulo(tipo, base, altura,raio));

                break;
            case 4:
                float lados[2];

                cout << "Cadastre a forma geometrica (triangulo):" << endl;

                cout << "Tipo: ";
                tipo = getString();

                cout << "Base: ";
                base = getInput<float>();

                cout << "Altura: ";
                altura = getInput<float>();

                cout << "Lado 1: ";
                lados[0] = getInput<float>();

                cout << "Lado 2: ";
                lados[1] = getInput<float>();

                cout << "Lado 3: ";
                lados[2] = getInput<float>();

                formasgeometricas.push_back(new triangulo(tipo, base, altura, lados[0], lados[1], lados[2]));
                triangulos.push_back(new triangulo(tipo, base, altura, lados[0], lados[1], lados[2]));

                break;
            case 5:
                cout << "Cadastre a forma geometrica (paralelogramo):" << endl;

                cout << "Tipo: ";
                tipo = getString();

                cout << "Base: ";
                base = getInput<float>();

                cout << "Altura: ";
                altura = getInput<float>();

                formasgeometricas.push_back(new paralelogramo(tipo, base, altura));
                paralelogramos.push_back(new paralelogramo(tipo, base, altura));
                break;
            case 6:
                float lado;
                cout << "Cadastre a forma geometrica (pentagono):" << endl;

                cout << "Tipo: ";
                tipo = getString();

                cout << "Lado: ";
                lado = getInput<float>();

                formasgeometricas.push_back(new pentagono(tipo, lado));
                pentagonos.push_back(new pentagono(tipo, lado));
                break;
            case 7:
                float ladohex;
                cout << "Cadastre a forma geometrica (hexagono):" << endl;

                cout << "Tipo: ";
                tipo = getString();

                cout << "Lado: ";
                ladohex = getInput<float>();

                formasgeometricas.push_back(new hexagono(tipo, ladohex));
                hexagonos.push_back(new hexagono(tipo, ladohex));
                break;
            case 8:
                cout << "Lista Geral" << endl;
                cout << "=====================================" << endl;
                for (FormaGeometrica * form: formasgeometricas){
                    form->imprime_parametros();
                    cout << "---------------------------------" << endl;
                }
                getchar();
                break;
            case 0:
                break;
            default:
                cout << "Opção inválida!" << endl;
        }

    }
    system("clear");
    return 0;
}
