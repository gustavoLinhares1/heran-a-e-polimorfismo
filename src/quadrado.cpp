#include "quadrado.hpp"
#include <iostream>
#include <math.h>

quadrado::quadrado(){
    set_tipo("Genérico");
    set_base(10.0f);
    set_altura(5.0);
}

quadrado::~quadrado(){
  std::cout << "Destrutor" << '\n';
}

quadrado::quadrado(string tipo, float base, float altura){
  set_tipo(tipo);
  set_base(base);
  set_altura(altura);
}
