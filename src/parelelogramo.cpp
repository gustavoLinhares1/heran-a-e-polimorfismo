#include "paralelogramo.hpp"
#include <iostream>
#include <math.h>

paralelogramo::paralelogramo(){
    set_tipo("Genérico");
    set_base(10.0f);
    set_altura(5.0);
}

paralelogramo::~paralelogramo(){
  std::cout << "Destrutor" << '\n';
}

paralelogramo::paralelogramo(string tipo, float base, float altura){
  set_tipo(tipo);
  set_base(base);
  set_altura(altura);
}
