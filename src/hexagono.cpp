#include "hexagono.hpp"
#include <iostream>
#include <math.h>

hexagono::hexagono(){
    set_tipo("Genérico");
    set_base(10.0f);
    set_altura(5.0);
}

hexagono::~hexagono(){
  std::cout << "Destrutor" << '\n';
}

hexagono::hexagono(string tipo, float lado){
  set_tipo(tipo);
  set_lado(lado);
}


void hexagono::set_lado(float lado){
  this->lado = lado;
}

float hexagono::get_lado(){
  return lado;
}

float hexagono::calcula_area(){
  return ((pow(lado,2)*sqrt(3)*3)/2);
}
float hexagono::calcula_perimetro(){
  return 6*lado;
}
void  hexagono::imprime_dados(){
    cout << "Tipo: " << get_tipo() << endl;
    cout << "Lado: " << get_lado() << endl;
    cout << "Area: " << calcula_area() << endl;
    cout << "Perimetro: " << calcula_perimetro() << endl;
}
