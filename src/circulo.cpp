#include "circulo.hpp"
#include <iostream>
#include <math.h>

circulo::circulo(){
  set_tipo("Genérico");
  set_base(10.0f);
  set_altura(5.0);
  set_raio(0.0);
}

circulo::~circulo(){
  std::cout << "Destrutor" << '\n';
}

circulo::circulo(string tipo, float base, float altura, float raio){
  set_tipo(tipo);
  set_base(base);
  set_altura(altura);
  set_raio(raio);
}


float circulo::calcula_area(){
    return raio * pow(M_PI,2.0);
}
float circulo::calcula_perimetro(){
    return 2*M_PI*raio;
}

void circulo::set_raio(float raio){
   if(raio < 0)
     throw(1);
   else
     this->raio = raio;
}

float circulo::get_raio(){
   return raio;
}

void circulo::imprime_parametros(){
    cout << "Tipo: " << get_tipo() << endl;
    cout << "Base: " << get_base() << endl;
    cout << "Altura: " << get_altura() << endl;
    cout << "Raio: " << get_raio() << endl;
    cout << "Area: " << calcula_area() << endl;
    cout << "Perimetro: " << calcula_perimetro() << endl;

}
