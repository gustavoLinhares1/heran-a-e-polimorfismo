#include "triangulo.hpp"
#include <iostream>
#include <math.h>

triangulo::triangulo(){
    set_tipo("Genérico");
    set_base(10.0f);
    set_altura(5.0);
    set_lados(0,0,0);
}

triangulo::~triangulo(){
  std::cout << "Destrutor" << '\n';
}

triangulo::triangulo(string tipo, float base, float altura, float lado1,float lado2,float lado3 ){
  set_tipo(tipo);
  set_base(base);
  set_altura(altura);
  set_lados(lado1,lado2,lado3);
}


float triangulo::calcula_area(){
  return (get_base()*get_altura())/2;

}
float triangulo::calcula_perimetro(){
  return (lado1+lado2+lado3);
}

void triangulo:: set_lados(float lado1,float lado2,float lado3){
  this->lado1 = lado1;
  this->lado2 = lado2;
  this->lado3 = lado3;
}


void triangulo::imprime_parametros(){
    cout << "Tipo: " << get_tipo() << endl;
    cout << "Base: " << get_base() << endl;
    cout << "Altura: " << get_altura() << endl;
    cout << "Area: " << calcula_area() << endl;
    cout << "Perimetro: " << calcula_perimetro() << endl;
}
