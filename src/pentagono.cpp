#include "pentagono.hpp"
#include <iostream>
#include <math.h>

pentagono::pentagono(){
    set_tipo("Genérico");
    set_base(10.0f);
    set_altura(5.0);
}

pentagono::~pentagono(){
  std::cout << "Destrutor" << '\n';
}

pentagono::pentagono(string tipo, float lado){
  set_tipo(tipo);
  set_lado(lado);
}


void pentagono::set_lado(float lado){
  this->lado = lado;
}

float pentagono::get_lado(){
  return lado;
}

float pentagono::calcula_area(){
  return ((pow(lado,2)*sqrt(25 +10*sqrt(5)))/4);
}
float pentagono::calcula_perimetro(){
  return 5*lado;
}
void  pentagono::imprime_dados(){
    cout << "Tipo: " << get_tipo() << endl;
    cout << "Lado: " << get_lado() << endl;
    cout << "Area: " << calcula_area() << endl;
    cout << "Perimetro: " << calcula_perimetro() << endl;
}
