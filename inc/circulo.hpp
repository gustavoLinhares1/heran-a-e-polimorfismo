#ifndef CIRCULO_HPP
#define CIRCULO_HPP

#include <string>
#include "formageometrica.hpp"
using namespace std;

class circulo : public FormaGeometrica {
private:
  float raio;
public:
  circulo();
  circulo(string tipo, float base, float altura, float raio);
  ~circulo();
  float calcula_area();
  float calcula_perimetro();
  void set_raio(float raio);
  float get_raio();
  void imprime_parametros();
};
#endif
