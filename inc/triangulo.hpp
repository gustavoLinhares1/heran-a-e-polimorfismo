#ifndef TRIANGULO_HPP
#define TRIANGULO_HPP

#include <string>
#include <vector>
#include "formageometrica.hpp"

using namespace std;

class triangulo : public FormaGeometrica{
private:
  float lado1,lado2,lado3;
public:
  triangulo();
  triangulo(string tipo, float base, float altura, float lado1,float lado2,float lado3 );
  ~triangulo();
  float calcula_area();
  void set_lados(float lado1,float lado2,float lado3);
  float calcula_perimetro();
  void imprime_parametros();
};
#endif
