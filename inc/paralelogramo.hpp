#ifndef PARALELOGRAMO_HPP
#define PARALELOGRAMO_HPP

#include <string>
#include "formageometrica.hpp"

using namespace std;

class paralelogramo : public FormaGeometrica{
private:

public:
  paralelogramo();
  paralelogramo(string tipo, float base, float altura);
  ~paralelogramo();
};
#endif
