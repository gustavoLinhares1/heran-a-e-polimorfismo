#ifndef PENTAGONO_HPP
#define PENTAGONO_HPP

#include <string>
#include "formageometrica.hpp"

using namespace std;

class pentagono : public FormaGeometrica{
private:
  float lado;
public:
  pentagono();
  pentagono(string tipo, float lado);
  ~pentagono();
  void set_lado(float lado);
  float get_lado();
  float calcula_area();
  float calcula_perimetro();
  void imprime_dados();
};
#endif
