#ifndef HEXAGONO_HPP
#define HEXAGONO_HPP

#include <string>
#include "formageometrica.hpp"

using namespace std;

class hexagono : public FormaGeometrica{
private:
  float lado;
public:
  hexagono();
  hexagono(string tipo, float lado);
  ~hexagono();
  void set_lado(float lado);
  float get_lado();
  float calcula_area();
  float calcula_perimetro();
  void imprime_dados();
};
#endif
