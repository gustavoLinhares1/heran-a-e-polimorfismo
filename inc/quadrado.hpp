#ifndef QUADRADO_HPP
#define QUADRADO_HPP

#include <string>
#include "formageometrica.hpp"

using namespace std;

class quadrado : public FormaGeometrica{
private:

public:
  quadrado();
  quadrado(string tipo, float base, float altura);
  ~quadrado();
  void imprime_dados();
};
#endif
